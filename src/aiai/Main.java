package aiai;

public class Main {

	public static String sayAiai(){
		return "アイアイ(アイアイ)";
	}

	public static String repeatMessage(String msg, int count){
			String repeatMsg="";
			for(int i =0; i<count; i++){
				repeatMsg+=msg;
			}
			return repeatMsg;
	}

	public static String sayLyric(String str){
		return str;
	}

	public static void main(String[] args) {
		String lyric="";
		lyric+=repeatMessage(sayAiai(),2);
		lyric+="\n"+repeatMessage(sayLyric("おさるさんだよ"),1);
		lyric+="\n"+repeatMessage(sayAiai(),2);
		lyric+="\n"+repeatMessage(sayLyric("みなみのしまの"),1);
		lyric+="\n"+repeatMessage(sayAiai(),4);
		lyric+="\n"+repeatMessage(sayLyric("しっぽのながい"),1);
		lyric+="\n"+repeatMessage(sayAiai(),2);
		lyric+="\n"+repeatMessage(sayLyric("おさるさんだよ"),1);
		System.out.println(lyric);
		lyric="";
		lyric+=repeatMessage(sayAiai(),2);
		lyric+="\n"+repeatMessage(sayLyric("おさるさんだね"),1);
		lyric+="\n"+repeatMessage(sayAiai(),2);
		lyric+="\n"+repeatMessage(sayLyric("きのはのおうち"),1);
		lyric+="\n"+repeatMessage(sayAiai(),4);
		lyric+="\n"+repeatMessage(sayLyric("おめめのまるい"),1);
		lyric+="\n"+repeatMessage(sayAiai(),2);
		lyric+="\n"+repeatMessage(sayLyric("おさるさんだね"),1);
		System.out.println(lyric);
	}

}